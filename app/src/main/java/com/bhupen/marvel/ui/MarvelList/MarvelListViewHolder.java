package com.bhupen.marvel.ui.MarvelList;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bhupen.marvel.R;

/**
 * Created by Bhupen on 25/05/2017.
 */

public class MarvelListViewHolder extends RecyclerView.ViewHolder {

    private ViewGroup container;
    private TextView marvelName;

    MarvelListViewHolder(View view) {
        super(view);
        container = (ViewGroup) view.findViewById(R.id.list_item_marvel_container);
        marvelName = (TextView) view.findViewById(R.id.list_item_marvel_name);
    }

    public ViewGroup getContainer() {
        return container;
    }

    public TextView getMarvelName() {
        return marvelName;
    }
}
