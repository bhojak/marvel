package com.bhupen.marvel.ui.MarvelDetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bhupen.marvel.R;
import com.bhupen.marvel.model.MarvelResponse;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Bhupen on 25/05/2017.
 */

public class MarvelDetailsActivity extends AppCompatActivity implements MarvelDetailsViewInterface {

    MarvelDetailsPresenterInterface presenter;

    public static final String EXTRA_NAME = "EXTRA_NAME";

    public static void launch(Context context, MarvelResponse marvelItem) {
        Intent intent = new Intent(context, MarvelDetailsActivity.class);
        intent.putExtra(EXTRA_NAME, marvelItem.getClass().getName());
        context.startActivity(intent);
    }

    @BindView(R.id.activity_marvel_name)
    TextView name;

    @BindView(R.id.activity_marvel_character)
    TextView name01;

    @BindView(R.id.activity_marvel_character_01)
    TextView desc;

    @BindView(R.id.activity_food_imageView)
    ImageView imageView;

    @BindView(R.id.activity_marvel_detail_progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marvel_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);

        String marvelId = getIntent().getStringExtra(EXTRA_NAME);

        presenter = new MarvelDetailsPresenterImpl();
        presenter.setView(this);
        presenter.getMarvelDetail(marvelId);
    }
    // impl interface methods
    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMarvel(MarvelResponse marvelItem) {


        //SET ALL TEXTVIEWS HERE
        name.setText("name");
        name01.setText("name01");
        desc.setText("desc");

       // imageView.setImageDrawable(ContextCompat.getDrawable(this, marvelItem.data.results.);
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, R.string.marvelItemError, Toast.LENGTH_SHORT).show();
    }
}
