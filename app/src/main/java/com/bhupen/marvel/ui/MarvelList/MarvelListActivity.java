package com.bhupen.marvel.ui.MarvelList;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bhupen.marvel.R;
import com.bhupen.marvel.model.MarvelResponse;
import com.bhupen.marvel.ui.MarvelDetails.MarvelDetailsActivity;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Bhupen on 25/05/2017.
 */

public class MarvelListActivity extends AppCompatActivity implements MarvelListViewInterface {


    MarvelListPresenterInterface presenter;

    private RecyclerView marvelRecyclerView;

    private LinearLayoutManager mLinearLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marvel_list);

        ButterKnife.bind(this);

        marvelRecyclerView = (RecyclerView) findViewById(R.id.activity_marvel_recyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        marvelRecyclerView.setLayoutManager(mLinearLayoutManager);

        presenter = new MarvelListPresenterImpl();
        presenter.setView(this);
        presenter.getMarvelList();
    }



    //Implement view interface methods
    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMarvelList(List<MarvelResponse> MarvelItemList) {
        marvelRecyclerView.setAdapter(new MarvelListAdapter(MarvelItemList));
        marvelRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, R.string.marvelListError, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void launchMarvelDetail(MarvelResponse marvelItem) {
        MarvelDetailsActivity.launch(this, marvelItem);
    }

      /*
   * Inner Classes
   */

    class MarvelListAdapter extends RecyclerView.Adapter<MarvelListViewHolder> {

        private List<MarvelResponse> marvelItemList;

        MarvelListAdapter(List<MarvelResponse> marvelItemList) {
            this.marvelItemList = marvelItemList;
        }

        @Override
        public MarvelListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(MarvelListActivity.this);
            return new MarvelListViewHolder(inflater.inflate(R.layout.list_item_marvel, parent, false));
        }

        @Override
        public void onBindViewHolder(MarvelListViewHolder holder, int position) {
            MarvelResponse marvelItem = marvelItemList.get(position);
            holder.getMarvelName().setText((marvelItem.getClass().getName()));
            holder.getContainer().setOnClickListener(v -> launchMarvelDetail(marvelItem));
        }

        @Override
        public int getItemCount() {
            return marvelItemList.size();
        }
    }
}
