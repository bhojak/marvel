package com.bhupen.marvel.ui.MarvelList;

import com.bhupen.marvel.app.Constants;
import com.bhupen.marvel.model.MarvelResponse;
import com.bhupen.marvel.network.MarvelApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Bhupen on 25/05/2017.
 */

public class MarvelListPresenterImpl implements MarvelListPresenterInterface {

    private MarvelListViewInterface view;



    @Override
    public void setView(MarvelListViewInterface view) {
        this.view = view;
    }

    @Override
    public void getMarvelList() {

        view.showLoading();

        Converter.Factory converter = GsonConverterFactory.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(converter)
                .build();

        MarvelApi marvelApi = retrofit.create(MarvelApi.class);

        marvelApi.getMarvelList().enqueue(new Callback<MarvelResponse>() {
            @Override
            public void onResponse(Call<MarvelResponse> call, Response<MarvelResponse> response) {

                if (response.code() != 200) {

                    view.showErrorMessage();

                } else {

                    List<MarvelResponse> marvelItemList = new ArrayList<MarvelResponse>();

                    view.showMarvelList(marvelItemList);
                }
                view.hideLoading();
            }

            @Override
            public void onFailure(Call<MarvelResponse> call, Throwable t) {
                view.showErrorMessage();
                view.hideLoading();
            }
        });
    }
}
