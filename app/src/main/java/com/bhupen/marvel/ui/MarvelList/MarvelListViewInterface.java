package com.bhupen.marvel.ui.MarvelList;

import com.bhupen.marvel.model.MarvelResponse;

import java.util.List;

/**
 * Created by Bhupen on 25/05/2017.
 */

public interface MarvelListViewInterface {

    void showLoading();

    void hideLoading();

    void showMarvelList(List<MarvelResponse> MarvelItemList);

    void showErrorMessage();

    void launchMarvelDetail(MarvelResponse marvelItem);
}
