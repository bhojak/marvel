package com.bhupen.marvel.ui.MarvelDetails;

/**
 * Created by Bhupen on 25/05/2017.
 */

public interface MarvelDetailsPresenterInterface {

    void setView(MarvelDetailsViewInterface view);

    void getMarvelDetail(String marvelDesc);
}
