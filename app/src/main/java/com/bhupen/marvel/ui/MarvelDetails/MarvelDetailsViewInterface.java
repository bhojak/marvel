package com.bhupen.marvel.ui.MarvelDetails;

import com.bhupen.marvel.model.MarvelResponse;

/**
 * Created by Bhupen on 25/05/2017.
 */

public interface MarvelDetailsViewInterface {

    void showLoading();

    void hideLoading();

    void showMarvel(MarvelResponse marvelItem);

    void showErrorMessage();
}
