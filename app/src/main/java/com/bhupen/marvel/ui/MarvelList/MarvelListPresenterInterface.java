package com.bhupen.marvel.ui.MarvelList;

/**
 * Created by Bhupen on 25/05/2017.
 */

public interface MarvelListPresenterInterface {

    void setView(MarvelListViewInterface view);

    void getMarvelList();
}
