package com.bhupen.marvel.app;

/**
 * Created by Bhupen on 25/05/2017.
 */

public class Constants {

    public static final String BASE_URL = "http://gateway.marvel.com:80/v1/public/";

    public static final String PUBLIC_API_KEY = "54306733de0f5cd1418aa05a85fa062a";

    public static final String PRIVATE_API_KEY = "5de1fabcda2ea08912bd8b09bca4321f50563655";

    public static final int SPLASH_TIMEOUT_SEC = 3 * 1000; //3 sec

    public static final int RECYCLER_VIEW_ITEM_SPACE = 48;

    public static final int API_RETRY_COUNT = 3;
}
