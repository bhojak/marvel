package com.bhupen.marvel.model;

//type (string, optional): A text identifier for the URL.,
//        url (string, optional): A full URL (including scheme, domain, and path).

import java.io.Serializable;

public class Url implements Serializable {

    public String type; // (string, optional): A text identifier for the URL.,
    public String url; // (string, optional): A full URL (including scheme, domain, and path).

}
