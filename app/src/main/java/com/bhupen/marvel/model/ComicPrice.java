package com.bhupen.marvel.model;

/**
 * Created by Bhupen on 25/05/2017.
 */

import java.io.Serializable;

public class ComicPrice implements Serializable {

    public String type; // A description of the price (e.g. print price, digital price).,
    public float price; // The price (all prices in USD).

}
