package com.bhupen.marvel.model;

/**
 * Created by Bhupen on 25/05/2017.
 */
import java.io.Serializable;

public class ComicSummary implements Serializable {

    public String resourceURI; //  The path to the individual comic resource.,
    public String name; // The canonical name of the comic.

}
