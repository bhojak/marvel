package com.bhupen.marvel.model;

/**
 * Created by Bhupen on 25/05/2017.
 */

import java.io.Serializable;
import java.util.Date;

public class ComicDate implements Serializable {

    public String type; // A description of the date (e.g. onsale date, FOC date).,
    public Date date; // The date.

}
