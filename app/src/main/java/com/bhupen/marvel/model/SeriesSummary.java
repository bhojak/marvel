package com.bhupen.marvel.model;

/**
 * Created by Bhupen on 25/05/2017.
 */

import java.io.Serializable;

public class SeriesSummary implements Serializable  {

    public String resourceURI; // The path to the individual series resource.,
    public String name; // The canonical name of the series.

}
