package com.bhupen.marvel.model;

import java.io.Serializable;
import java.util.List;


//
//        offset (int, optional): The requested offset (number of skipped results) of the call.,
//        limit (int, optional): The requested result limit.,
//        total (int, optional): The total number of resources available given the current filter set.,
//        count (int, optional): The total number of results returned by this call.,
//        results (Array[Comic], optional): The list of comics returned by the call

public class Data implements Serializable {

    public Integer offset; // The requested offset (number of skipped results) of the call.,
    public Integer limit; // The requested result limit.,
    public Integer total; // The total number of resources available given the current filter set.,
    public Integer count; // The total number of results returned by this call.,
    public List<Comic> results; // The list of comics returned by the call


}
