package com.bhupen.marvel.model;


import java.io.Serializable;

public class EventSummary implements Serializable {

    public String resourceURI; // The path to the individual event resource.,
    public String name; // The name of the event.

}
