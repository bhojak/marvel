package com.bhupen.marvel.network;

import com.bhupen.marvel.app.Constants;
import com.bhupen.marvel.model.MarvelResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Bhupen on 25/05/2017.
 */

public interface MarvelApi {

    @GET("comics?apikey=" + Constants.PUBLIC_API_KEY)
    Call<MarvelResponse> getMarvelList();

    @GET("/v1/public/comics")
    Call<MarvelResponse> getComicList(@Query("orderBy") String orderBy,
                                        @Query("limit") String limit,
                                        @Query("offset") String offset,
                                        @Query("ts") String ts,
                                        @Query("apikey") String apiKey,
                                        @Query("hash") String hash);
}
